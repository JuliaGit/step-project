
$(document).ready(function(){

$(function() {
  $('ul.services-name').on('click', 'li:not(.services-name-item-acteve)', function() {
    $(this)
      .addClass('services-name-item-acteve').siblings().removeClass('services-name-item-acteve')
      .closest('div.services-tabs').find('div.services-tabs-content').removeClass('services-tabs-content-active').eq($(this).index()).addClass('services-tabs-content-active');
  });

});


$('.work-name-item').on('click', function() {
    $('.work-name-item-acteve').removeClass('work-name-item-acteve');
    $(this).addClass('work-name-item-acteve');
    $('.work-gallery-item').addClass('all').hide();
    let imgClass = $(this).attr('data-tab');
    $('.' + imgClass).show();
} );

$(".work-name-item:eq(0)").click();


$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: false,
  focusOnSelect: true
});

});





